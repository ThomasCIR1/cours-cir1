<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title> Chat </title>
    <link rel="stylesheet" href="../style.css">
  </head>

  <body>
  <?php if(!empty($reponse)){ ?>
    <table>
      <tr> <th>Auteur</th> <th>Message</th> <th>Date</th> </tr>
      <?php
      foreach($reponse as $row){ ?>
        <tr>
          <td> <?php echo $row['auteur']; ?> </td>
          <td class="message"> <?php afficherMessage($row['message'], $bdd); ?> </td>
          <td> <?php echo $row['date']; ?> </td>
        </tr>
      <?php
      }
      $reponse->closeCursor();?>
    </table>
    <?php }
    ?>
    <form method="post" action="../controllers/chat.php">
      <label for="auteur">Auteur :</label><input type="text" name="auteur" id="auteur">
      <label for="message">Votre message :</label><textarea name="message" id="message" rows="10" cols="100"></textarea>
      <input type="submit" value="Envoyer">
    </form>
  </body>
