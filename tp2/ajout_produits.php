<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Ajout produit</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <?php if(isset($_POST['nom'])){
      echo '<p> Produit enregistré </p>';
      define('TARGET_DIRECTORY', './');
      move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
      $fichier = fopen('mesproduits.csv', 'a+');
      fputs($fichier, $_POST['nom'] . ';' . $_POST['prix'] . ';' . $_POST['nbproduits'] . ';' . $_FILES['photo']['name'] . ';');
      fclose($fichier);
    }
    else{ ?>
    <form action="ajout_produits.php" method="POST" enctype="multipart/form-data">
      <label for="nom">Nom du produit : </label><input type="text" name="nom" id="nom" placeholder="Ex : lave-vaiselle" pattern="[a-zA-Z0-9]+" autofocus required>
      <label for="prix">Prix du produit : </label><input type="number" name="prix" id="prix" min="0" step="0.01" placeholder="Ex : 0.58" required>
      <label for="nbproduits">Nombre de produits: </label><input type="number" name="nbproduits" id="nbproduits" placeholder="Ex : 5" required min="0">
      <label for="photo">Photo du produit : </label><input type="file" name="photo" id="photo" required>
      <input type="submit" value="Envoyer">
    </form>
  <?php } ?>
  </body>
</html>
