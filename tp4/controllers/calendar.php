<?php
include('models/events.php');
if(isset($_SESSION['userInfo'])){
  $user = $_SESSION['userInfo'];
  $events = getEventsFromUser($bdd, $user);
}
else{
  header('Location: index.php?page=connection');
}

if(isset($_GET['date'])){
  $yearMonth = $_GET['date'];
}
else{
  $yearMonth = date('Y-m');
}

$timestamp = strtotime($yearMonth.'-01');

if ($timestamp === false) {
    $timestamp = time();
}

$today = date('Y-m-d');
$firstday = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));

$previousWeek = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
$nextWeek = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));

$numberOfDays = date('t', $timestamp);

include('views/calendar.php');
