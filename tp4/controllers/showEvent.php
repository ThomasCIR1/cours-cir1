<?php
include('models/events.php');
if(isset($_SESSION['userInfo'])){
  if(isset($_GET['event'])){
    $event = getEvent($bdd, $_GET['event']);
    setlocale(LC_TIME, 'french');
    if(isset($_POST['inscription'])){
      if(!isUserParticipatingEvent($bdd, $event, $_SESSION['userInfo'])){
        updateEventPlaces($bdd, $event);
        addUserInEvent($bdd, $event, $_SESSION['userInfo']);
        $event = getEvent($bdd, $_GET['event']);
        $message = 'Votre inscription à cet événement a été prise en compte.';
      }
      else{
        $message = 'Vous participez déjà à cet événement.';
      }
    }
    if(isset($_POST['annuler'])){
      deleteEvent($bdd, $_GET['event']);
      
    }
  }
  else{
    header('Location: index.php?page=calendar');
  }
}
else{
  header('Location: index.php?page=connection');
}
include('views/showEvent.php');
