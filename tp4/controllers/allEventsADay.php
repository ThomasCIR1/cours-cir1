<?php
include('models/events.php');
if(isset($_SESSION['userInfo'])){
  $user = $_SESSION['userInfo'];
  $events = getEventsFromUser($bdd, $user);
}
else{
  header('Location: index.php?page=connection');
}
include('views/allEventsADay.php');
