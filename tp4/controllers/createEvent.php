<?php
include('models/events.php');
if(isset($_SESSION['userInfo'])){
  $user = $_SESSION['userInfo'];
  if($user['rank'] != 'ORGANIZER'){
    header('Location: index.php');
  }
}
else{
  header('Location: index.php?page=connection');
}
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['startDate']) && isset($_POST['endDate']) && isset($_POST['numberOfPlaces'])){
  $date = filter_input(INPUT_POST, 'startDate', FILTER_CALLBACK, ['options' => 'filterDate']);
  $date2 = filter_input(INPUT_POST, 'endDate', FILTER_CALLBACK, ['options' => 'filterDate']);
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
  $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_SPECIAL_CHARS);
  $numberOfPlaces = filter_input(INPUT_POST, 'numberOfPlaces', FILTER_SANITIZE_NUMBER_INT);
  if (!$date) {
    // error, bad date
  }
  else{
    addEvent($bdd, $name, $description, $date, $date2, $numberOfPlaces, $user);
    header('Location: index.php?page=calendar');
  }
}
include('views/createEvent.php');
