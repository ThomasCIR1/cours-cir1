<?php
if(isset($_POST['login']) && isset($_POST['password'])){
  $request = $bdd->prepare('SELECT * FROM Users WHERE login = :login');
  $request->execute(array(
    'login' => $_POST['login']
  ));
  $answer = $request->fetch(PDO::FETCH_ASSOC);
}
if(isset($answer)){
  if(!$answer){
    echo 'Mauvais identifiant<br>';
  }
  else{
    if(password_verify($_POST['password'], $answer['password'])){
      $_SESSION['userInfo'] = $answer;
      header('Location: index.php?page=calendar');
    }
    else{
      echo 'Mot de passe incorrect<br>';
    }
  }
}
include('views/connexion.php');
