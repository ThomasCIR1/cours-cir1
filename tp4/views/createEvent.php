<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Créer un événement</title>
    <link rel="stylesheet" href="css/createEvent.css">
    <link rel="stylesheet" href="css/calendar.css">
  </head>

  <body>
    <?php include('header.php'); ?>
    <form method="post" action="index.php?page=createEvent">
      <label for="name">Nom de l'événement</label><input type="text" id="name" name="name">
      <label for="description">Description de l'événement</label><input type="textarea" id="description" name="description">
      <label for="startDate">Date de début</label><input type="date" id="startDate" name="startDate">
      <label for="endDate">Date de fin</label><input type="date" id="endDate" name="endDate">
      <label for="numberOfPlaces">Nombre de places</label><input type="text" id="numberOfPlaces" name="numberOfPlaces">
      <button type="submit">Créer</button>
    </form>
  </body>
</html>
