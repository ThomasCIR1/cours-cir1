<header>
  <?php
  if(isset($_SESSION['userInfo'])){
    $user = $_SESSION['userInfo']; ?>
    <div class="topnav">
      <a href="index.php">Calendrier</a>
      <?php if($user['rank'] == 'ORGANIZER'){ ?>
        <a href="index.php?page=createEvent">Créer un événement</a>
      <?php } ?>
      <a href="index.php?page=deconnection">Déconnexion</a>
    </div>
  <?php } ?>
</header>
