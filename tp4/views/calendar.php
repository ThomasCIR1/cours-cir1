<html lang ="fr">
  <head>
    <meta charset="utf-8">
    <title>Calendrier</title>
    <link rel="stylesheet" href="css/calendar.css">
  </head>

  <body>
    <?php include('views/header.php'); ?>
    <table>
      <caption>
        <p><a href="index.php?page=calendar&date=<?=$previousWeek?>">&lt;</a>
        <?=$yearMonth?>
        <a href="index.php?page=calendar&date=<?=$nextWeek?>">&gt;</a></p>
      </caption>

      <thead>
        <tr>
          <th>Dimanche</th>
          <th>Lundi</th>
          <th>Mardi</th>
          <th>Mercredi</th>
          <th>Jeudi</th>
          <th>Vendredi</th>
          <th>Samedi</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <?php for($j = 0; $j < $firstday; $j++){ ?>
          <td></td>
        <?php } ?>
        <?php for($i = $firstday; $i < $numberOfDays + $firstday; $i++){
          if($i % 7 == 0){ ?>
            <tr>
          <?php } ?>
              <td>
                <div class="events">
                  <div class="numDay">
                    <?=$i - $firstday + 1?>
                    <hr style="width: 100%; color: rgba(200,200,200,1)">
                  </div>
                  <?php if(($i - $firstday + 1) < 10){
                    $dateCurrentDay = $yearMonth . '-0' . ($i-$firstday+1);
                  }
                  else{
                    $dateCurrentDay = $yearMonth . '-' . ($i-$firstday+1);
                  }
                  $numberOfEventsInDay = 0;
                  foreach($events as $key => $event){
                    $startDateEvent = DateTime::createFromFormat('Y-m-d H:i:s', $event['startdate']);
                    $endDateEvent = DateTime::createFromFormat('Y-m-d H:i:s', $event['enddate']);
                    if(strtotime($dateCurrentDay) >= strtotime($startDateEvent->format('Y-m-d')) && strtotime($dateCurrentDay) <= strtotime($endDateEvent->format('Y-m-d'))){
                      if($numberOfEventsInDay < 5){ ?>
                        <a href="index.php?page=showEvent&event=<?=$event['id']?>"><?=$event['name']?></a>
                      <?php }
                      $numberOfEventsInDay++;
                    }
                    if($numberOfEventsInDay == 5){ ?>
                      <a href="index.php?page=eventsOneDay&date=<?=$dateCurrentDay?>">Voir plus</a>
                    <?php }
                  } ?>
                </div>
              </td>

        <?php } ?>
      </tbody>
    </table>
  </body>
</html>
