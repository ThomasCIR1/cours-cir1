<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title><?=$event['name']?></title>
  </head>

  <body>
    <?php include('views/header.php'); ?>
    <div class="event">
      <?php if(isset($message)){
        echo $message;
      } ?>
      <form method="post" action="index.php?page=showEvent&event=<?=$event['id']?>">
        <p>Nom de l'évenement : <?=$event['name']?></p>
        <p>Description : <?=$event['description']?></p>
        <p>Date de début : <?=strftime('%A %e %B %Y à %kh%M', strtotime($event['startdate']))?></p>
        <p>Date de fin : <?=strftime('%A %e %B %Y à %kh%M', strtotime($event['enddate']))?></p>
        <p>Nom du créateur : <?=getCreateurFromEvent($bdd, $event)?></p>
        <p>Nombre de places disponibles : <?=$event['nb_place']?></p>
        <?php if($user['rank'] == 'CUSTOMER'){ ?>
          <input type="submit" name="inscription" value="S'inscrire">
        <?php }
        else if($user['rank'] == 'ORGANIZER'){ ?>
          <input type="submit" name="annuler" value="Annuler">
        <?php } ?>
      </form>
    </div>
  </body>
</html>
