<?php
session_start();
include('models/sql_connection.php');
if(isset($_GET['page'])){
  if($_GET['page'] == 'createEvent'){
    include('controllers/createEvent.php');
  }
  else if($_GET['page'] == 'deconnection'){
    session_destroy();
    header('Location: index.php?page=connection');
  }
  else if($_GET['page'] == 'connection'){
    include('controllers/connexion.php');
  }
  else if($_GET['page'] == 'calendar'){
    include('controllers/calendar.php');
  }
  else if($_GET['page'] == 'eventsOneDay'){
    include('controllers/allEventsADay.php');
  }
  else if($_GET['page'] == 'showEvent'){
    include('controllers/showEvent.php');
  }
  else{
    include('controllers/calendar.php');
  }
}
else{
  include('controllers/calendar.php');
}
