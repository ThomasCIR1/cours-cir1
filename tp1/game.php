<?php
<<<<<<< HEAD
/* Fonction qui initialise le jeu, cellules vivantes à la première génération */
function initCells($square_size, $array_plateau){
    for($i = 0; $i < $square_size; $i++){
      for($j = 0; $j < $square_size; $j++){
        $randomNumber = rand(0, 1);
        if($randomNumber == 0){
          $array_plateau[$i][$j] = 'o';
        }
        else{
          $array_plateau[$i][$j] = ' ';
        }
      }
    }
    return $array_plateau;
}

/* Fonction d'affichage du jeu */
function printGame($square_size, $array_plateau){
  echo '<table>';
  for($i = 0; $i < $square_size; $i++){
    echo '<tr>';
    for($j = 0; $j < $square_size; $j++){
      echo '<td>';
      echo $array_plateau[$i][$j];
      echo '</td>';
    }
    echo '</tr>';
  }
}

function countNeighbours($linePosition, $columnPosition, $array_plateau){
  $numberOfNeighbours = 0;
  if(isset($array_plateau[$linePosition - 1][$columnPosition])){
    if($array_plateau[$linePosition - 1][$columnPosition] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition - 1][$columnPosition - 1])){
    if($array_plateau[$linePosition - 1][$columnPosition - 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition - 1][$columnPosition + 1])){
    if($array_plateau[$linePosition - 1][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition][$columnPosition - 1])){
    if($array_plateau[$linePosition][$columnPosition - 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition][$columnPosition + 1])){
    if($array_plateau[$linePosition][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition + 1][$columnPosition - 1])){
    if($array_plateau[$linePosition + 1][$columnPosition - 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition + 1][$columnPosition])){
    if($array_plateau[$linePosition + 1][$columnPosition] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($array_plateau[$linePosition + 1][$columnPosition + 1])){
    if($array_plateau[$linePosition + 1][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  return $numberOfNeighbours;
}

function nextGen($square_size, $array_plateau){
  /* Initialisation du tableau de la génération suivante */
  $array_nextGen = array();
  for($i = 0; $i < $square_size; $i++){
    $array_nextGen[$i] = array();
  }
  /* Remplissage du prochain tableau de jeu */
  for($i = 0; $i < $square_size; $i++){
    for($j = 0; $j < $square_size; $j++){
      if($array_plateau[$i][$j] == 'o'){
        if(countNeighbours($i, $j, $array_plateau) == 2 || countNeighbours($i, $j, $array_plateau) == 3){
          $array_nextGen[$i][$j] = 'o';
        }
        else{
          $array_nextGen[$i][$j] = ' ';
        }
      }
      else{
        if(countNeighbours($i, $j, $array_plateau) == 3){
          $array_nextGen[$i][$j] = 'o';
        }
        else{
          $array_nextGen[$i][$j] = ' ';
        }
      }
    }
  }
  return $array_nextGen;
=======
function initializeGame($nbOfRow, $nbOfCol) {
	$game = [];
	for($i = 0; $i < $nbOfRow; $i++) {
		$game[] = [];
		for($j = 0; $j < $nbOfCol; $j++) {
			$game[$i][$j] = (bool) random_int(0, 1);
		}
	}
	return $game;
}


function countNeightboors($game, $row, $col) {
	$neigboorsCoordinates = [[$row - 1, $col - 1], [$row - 1, $col], [$row - 1, $col + 1],
	                         [$row, $col - 1], [$row, $col + 1],
	                         [$row + 1, $col - 1], [$row + 1, $col], [$row + 1, $col + 1]];
	$neighboors = 0;
	foreach($neigboorsCoordinates as $coordinates) {
		if (isset($game[$coordinates[0]]) && isset($game[$coordinates[0]][$coordinates[1]])) {
			$neighboors += $game[$coordinates[0]][$coordinates[1]];
		}
	}
	return $neighboors;
}


function birth($array, $row, $col) {
	return (int) (countNeightboors($array, $row, $col) == 3);
}

function death($array, $row, $col) {
	return (int) !(countNeightboors($array, $row, $col) >= 4 || countNeightboors($array, $row, $col) <= 1);
}

function nextGeneration($oldGeneration) {
	$newGeneration = initializeGame(count($oldGeneration), count($oldGeneration[0]));
	foreach($oldGeneration as $rowIndex => $row) {
		foreach($row as $colIndex => $value) {
			$newGeneration[$rowIndex][$colIndex] = $value ? death($oldGeneration, $rowIndex, $colIndex) : birth($oldGeneration, $rowIndex, $colIndex);
		}
	}
	return $newGeneration;
}

function gameIsStill($old, $new) {
	foreach($old as $rowIndex => $row) {
		if (is_array($row) && !gameIsStill($row, $new[$rowIndex])) {
			return false;
		} else if (!is_array($row) && $row != $new[$rowIndex]) {
			return false;
		}
	}
	return true;
>>>>>>> cd02f0c74c48f9bd3e16f1d0b35e660ca2ae32f9
}
